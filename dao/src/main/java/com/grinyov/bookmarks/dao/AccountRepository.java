package com.grinyov.bookmarks.dao;

import com.grinyov.bookmarks.dao.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

/**
 * Created by grinyov
 */
public interface AccountRepository extends JpaRepository<Account, Long> {
    Optional<Account> findByUsername(String username);
}